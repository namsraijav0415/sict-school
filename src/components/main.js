import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LandingPage from './landingpage';
import AboutMe from './aboutme';
import Contact from './contact';
import Projects from './projects';
import Resume from './resume';
import Home from './Home';
import Zahirgaa from './zahirgaa';
import Surgalt from './surgalt';
import Erdem from './erdem';
import { Login } from './login';


const Main = () => (
  <Switch>
    <Route exact path="/" component={Login} />
    <Route path="/aboutme" component={AboutMe} />
    <Route path="/contact" component={Contact} />
    <Route path="/projects" component={Projects} />
    <Route path="/resume" component={Resume} />
    <Route path="/Home" component={Home} />
    <Route path="/zahirgaa" component={Zahirgaa} />
    <Route path="/surgalt" component={Surgalt} />
    <Route path="/erdem" component={Erdem} />
    <Route path="/login" component={Login} />

  </Switch>
)

export default Main;
