import React, { Component } from 'react';
import StickyFooter from 'react-sticky-footer';
const shutis = require('../assets/shutis.png');


class About extends Component {
  render() {
    return (
      <div class="full-height">
        <div className="secondaoubdiv">
        <div className="ulmedehabout">

          <div className="headeabout" >
              <a style={{ marginLeft: 20, color: 'blue', fontSize: 12, }} >Нүүр </a>
              <text style={{ marginLeft: 5, fontSize: 12, color: 'gray' }}>   / Бидний тухай </text>
            </div>
            <div className="headeaboutText" >
              <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                1967 онд МУИС-ийн Энерги механикийн факультет  холбооны инженер мэргэжлээр анхны элсэлт авснаар энэ хөтөлбөрөөр мэргэжилтэн бэлтгэх үндэс суурь тавигдсан. Улмаар 1971 оны 2-р сард МУИС-ийн Эрчим хүчний факультетийн харъяанд Радио холбооны тэнхим албан ёсоор байгуулагдаж 6 багштайгаар үйл ажиллагаа явуулж эхэлсэн түүхтэй.
          </text><br />
              <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                Радио холбооны тэнхим нь тухайн үед улс орны бүтээн байгуулалт, шинээр нэвтрүүлж байгаа техник, технологитой уялдуулан радио холбоо, цахилгаан холбоо гэсэн 2 хөтөлбөрөөр сургалт явуулж байсан ба 1986 онд Инженер-эдийн засгийн факультеттэй нэгдэн Мэдээллийн техник автоматжуулалтын факультет (МТАФ) болж өргөжсөн байна.
          </text><br />
              <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                1960 оноос радио өргөн нэвтрүүлгийн их чадлын станцууд, 1967 онд анхны телевизийн систем, 1971 онд сансрын холбооны анхны газрын станц, 1981 оноос улсын хэмжээний радио релейний сүлжээ гэх мэт бүтээн байгуулалтууд хийгдсэнээр инженер технологийн мэргэжилтний хэрэгцээ улам өсөн нэмэгдсэн байна.
          </text><br />
              <text style={{ marginLeft: 17, fontSize: 15, textAlign: 'justify', lineHeight: 1.5 }}>
                1991 онд Мэдээллийн техник автоматжуулалтын факультет (МТАФ) нь Холбооны сургалтын төв/Холбооны коллеж/-тэй нэгдэж хуучнаар Техникийн Их Сургууль (ТИС)-ийн бүрэлдэхүүнд “Мэдээллийн Техникийн Сургууль(МТС)” нэртэй сургууль байгуулагдсан. 1997 онд сургуулийн нэрийг  Холбоо Мэдээллийн Техникийн Сургууль болгосон бол 2001 оноос Шинжлэх Ухаан технологийн Сургууль(ШУТИС)-ийн бүрэлдэхүүнд өнөөгийн Мэдээлэл, Холбооны Технологийн Сургууль  бий болсон юм.
          </text>
            </div>
          </div>
          <div className="garchig" >
            <MenuData data={data} />
          </div>
        </div>
        {/* <div className="footercontainer">  */}
        <StickyFooter
          bottomThreshold={50}
          normalStyles={{
            backgroundColor: "#02459e",
            padding: "2rem",
            height: '400',
          }}
          stickyStyles={{
            backgroundColor: '#833ab4',
            padding: "2rem",
            height: 400,
            flexDirection: 'row',
          }}
        >
          <div className="footerGod">
            <div className="footerfirstdiv">
              <img
                // src="https://www.shareicon.net/download/2015/09/18/103157_man_512x512.png"
                src={shutis}
                alt="avatar"
                style={{ height: '100px', width: '50px' }}
              />
              <text style={{ marginTop: 10, color: 'white', fontSize: 18 }} >Монгол Улсын Шинжлэх Ухаан Технологийн Их Сургууль</text>

            </div>
            <div className="secondfooterfirstdiv">
              <text style={{ fontSize: 20, color: 'yellow' }}>
                Үндсэн цэс
          </text>
              <text style={{ marginTop: 10, color: 'white', fontSize: 13 }} >Нүүр  /  Бидний тухай  /  Захиргаа  /  Сургалт  /  Эрдэм шинжилгээ  /  Салбарууд  /  Оюутан</text>

            </div>
            <div className="secondfooterfirstdiv">
              <text style={{ fontSize: 20, color: 'yellow' }}>
                Бидэнтэй холбогдох
          </text>
              <text style={{ marginTop: 10, color: 'white', fontSize: 13 }} >Монгол улс, Улаанбаатар хот
              Баянзүрх дүүрэг,22-р хороо
              Утас: 976-70151333
              Сургалтын алба: 70152333</text>
            </div>
          </div>
        </StickyFooter>
        {/* </div> */}
      </div>
    )
  }
}

export default About;

const data = ['Захирлын мэндчилгээ','алсын хараа', 'Эрхэм зорилго', 'Товч түүх','Сургуулийн танилцуулга'];
function MenuData({data}) {
  return <div style={{marginTop: 15}}>
    <div className="menuData"><text style={{color: '#5D6D7E'}}>Бидний тухай</text></div>
    {data.map((item, index)=> {
      return <div className="menuData"><text className="myFontText">{item}</text></div>
    })}
  </div>
}